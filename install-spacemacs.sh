#!/bin/bash

sudo apt install -y libncurses-dev
cd ~
wget https://mirrors.tuna.tsinghua.edu.cn/gnu/emacs/emacs-26.1.tar.xz
tar xf emacs-26.1.tar.xz && rm emacs-26.1.tar.xz
cd emacs-26.1
./configure --with-gnutls=no
sudo make -j4 install

git clone -b develop https://github.com/syl20bnr/spacemacs ~/.emacs.d
cd ~
