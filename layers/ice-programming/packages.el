;;; packages.el --- ice-programming layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: jackie <zhangk1991@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defconst ice-programming-packages
  '(
    ;; ccls
    )
  )

;; (defun ice-programming/init-ccls ()
;;   (use-package ccls
;;     :init
;;     (progn
;;       (spacemacs/add-to-hooks #'lsp-ccls-enable '(c-mode-hook c++-mode-hook))
;;       )))

;;; packages.el ends here
