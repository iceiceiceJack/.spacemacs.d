;;; packages.el --- ice-org layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: jackie <zhangk1991@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(defconst ice-org-packages
  '(
    (org :location built-in)
    org-pomodoro
    deft
    )
  )

(defun ice-org/post-init-org ()
  (add-hook 'org-mode-hook (lambda () (spacemacs/toggle-line-numbers-off)) 'append)
  (with-eval-after-load 'org
    (progn
      (spacemacs/set-leader-keys-for-major-mode 'org-mode "," 'org-priority)
      (require 'org-compat)
      (require 'org)
      (add-to-list 'org-modules 'org-habit)
      (require 'org-habit)

      (setq org-refile-use-outline-path 'file)
      (setq org-outline-path-complete-in-steps nil)
      (setq org-refile-targets
            '((nil :maxlevel . 4)
              (org-agenda-files :maxlevel . 4)))

      (setq org-bullets-bullet-list '("☰" "☷" "☯" "☭"))

      ;; config stuck project
      (setq org-stuck-projects '("TODO={.+}/-DONE" nil nil "SCHEDULED:\\|DEADLINE:"))

      ;; Org clock
      (setq org-clock-in-switch-to-state "STARTED")
      (setq org-clock-into-drawer t)
      (setq org-clock-out-remove-zero-time-clocks t)

      ;; todo keywords
      (setq org-todo-keywords
            (quote ((sequence "TODO(t!)" "|" "DONE(d!)")
                    (sequence "STARTED(s)" "WAITING(w@/!)" "SOMEDAY(S)" "|" "CANCELLED(c@/!)"))))

      ;; agenda
      (setq org-agenda-inhibit-startup t) ;; ~50x speedup
      (setq org-agenda-span 'day)
      ;; (setq org-agenda-use-tag-inheritance nil) ;; 3-4x speedup
      (setq org-agenda-window-setup 'only-window)
      (setq org-log-done t)

      ;; define the refile targets
      (setq org-agenda-file-note (expand-file-name "notes.org" org-agenda-dir))
      (setq org-agenda-file-gtd (expand-file-name "gtd.org" org-agenda-dir))
      (setq org-agenda-file-journal (expand-file-name "journal.org" org-agenda-dir))
      (setq org-agenda-file-code-snippet (expand-file-name "snippet.org" org-agenda-dir))
      (setq org-default-notes-file (expand-file-name "gtd.org" org-agenda-dir))
      (setq org-agenda-files (list org-agenda-dir))
      (setq org-agenda-text-search-extra-files '(agenda-archives))

      (setq org-agenda-custom-commands
            '(
              ("w" . "Task")
              ("wa" "important and urgent!!!" tags-todo "-weekly-monthly-daily+PRIORITY=\"A\"")
              ("wb" "just important" tags-todo "-weekly-monthly-daily+PRIORITY=\"B\"")
              ("wc" "nothing serious" tags-todo "+PRIORITY=\"C\"")
              ("p" . "Project")
              ("pl" "leftovers" tags-todo "-weekly-monthly-daily+leftovers+CATEGORY=\"work\"")
              ("pr" "robot" tags-todo "-weekly-monthly-daily+robot+CATEGORY=\"work\"")
              ("pa" "aistore" tags-todo "-weekly-monthly-daily+aistore+CATEGORY=\"work\"")
              ("l" "Life" tags-todo "+CATEGORY=\"life\"")
              ("d" "Dreams" tags-todo "+CATEGORY=\"dream\"")
              ("W" "Weekly Review"
               ((stuck "") ;; review stuck projects as designated by org-stuck-projects
                (tags-todo "-weekly-monthly-daily+leftovers+CATEGORY=\"work\"")
                (tags-todo "-weekly-monthly-daily+robot+CATEGORY=\"work\"")
                (tags-todo "-weekly-monthly-daily+aistore+CATEGORY=\"work\"")
                ))
              ))

      ;; capture
      (setq org-capture-templates
            '(("t" "Todo" entry
               (file+headline org-agenda-file-gtd "Capture")
               "* TODO [#B] %?\n  :PROPERTIES:\n  :CATEGORY: \n  :END:\n   %i\n %U"
               :empty-lines 1)
              ("d" "Dream" entry
               (file+headline org-agenda-file-gtd "Dream")
               "* TODO [#B] %?\n  :PROPERTIES:\n  :CATEGORY: dream\n  :END:\n   %i\n %U"
               :empty-lines 1)
              ("n" "Notes" entry
               (file+headline org-agenda-file-note "Quick Notes")
               "* %?\n  %i\n %U"
               :empty-lines 1)
              ("s" "Code Snippet" entry
               (file org-agenda-file-code-snippet)
               "* %?\t%^g\n#+BEGIN_SRC %^{language}\n\n#+END_SRC")
              ("l" "Links" entry
               (file+headline org-agenda-file-note "Quick Notes")
               "* TODO [#C] %?\n  %i\n %a \n %U"
               :empty-lines 1)
              ("j" "Journal Entry" entry
               (file+datetree org-agenda-file-journal)
               "* %?"
               :empty-lines 1)
              ("p" "Project")
              ("pa" "Aistore" entry
               (file+headline org-agenda-file-gtd "Projects")
               "* TODO [#A] %? :aistore:\n  :PROPERTIES:\n  :CATEGORY: work\n  :END:\n   %i\n %U"
               :empty-lines 1)
              ("pl" "Leftovers" entry
               (file+headline org-agenda-file-gtd "Projects")
               "* TODO [#A] %? :leftovers:\n  :PROPERTIES:\n  :CATEGORY: work\n  :END:\n   %i\n %U"
               :empty-lines 1)
              ("pr" "Robot" entry
               (file+headline org-agenda-file-gtd "Projects")
               "* TODO [#A] %? :robot:\n  :PROPERTIES:\n  :CATEGORY: work\n  :END:\n   %i\n %U"
               :empty-lines 1)
              ))

      ;;reset subtask
      (setq org-default-properties (cons "RESET_SUBTASKS" org-default-properties))

      ;; babel
      (org-babel-do-load-languages
       'org-babel-load-languages
       '(
         (shell . t)
         (dot . t)
         (js . t)
         (latex .t)
         (python . t)
         (emacs-lisp . t)
         (C . t)
         ))

      (with-eval-after-load 'org-agenda
        (define-key org-agenda-mode-map (kbd "P") 'org-pomodoro)
        (spacemacs/set-leader-keys-for-major-mode 'org-agenda-mode
          "." 'spacemacs/org-agenda-transient-state/body)
        )
      ;; (setq org-tags-match-list-sublevels nil)
      (add-hook 'org-mode-hook
                '(lambda ()
                   (local-set-key (kbd "C-c i s") 'iceiceice/org-insert-src-block))
                '(lambda()
	                 (setq truncate-lines nil))
                (spacemacs/set-leader-keys-for-major-mode 'org-mode
                  "sa" 'org-archive-to-archive-sibling)
                )
      (add-hook 'org-after-todo-statistics-hook 'iceiceice/org-summary-todo)

      ;; 加密文章
      ;; "http://coldnew.github.io/blog/2013/07/13_5b094.html"
      (require 'org-crypt)
      (org-crypt-use-before-save-magic)
      (setq org-crypt-tag-matcher "secret")
      (setq org-tags-exclude-from-inheritance (quote ("secret")))
      (setq org-crypt-key nil)
      )))

(defun ice-org/post-init-org-pomodoro ()
  (iceiceice/pomodoro-notification))

(defun ice-org/post-init-deft ()
  (progn
    (setq deft-use-filter-string-for-filename t)
    (setq deft-recursive t)
    (setq deft-extension "org")
    (setq deft-directory deft-dir)))

;;; packages.el ends here
