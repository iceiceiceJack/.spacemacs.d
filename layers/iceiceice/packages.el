;;; packages.el --- iceiceice layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author: jackie <zhangk1991@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3


(defconst iceiceice-packages
  '()
  )

(configuration-layer/declare-layers '(
                                      ice-misc
                                      ice-programming
                                      ice-org
                                      ))
;;; packages.el ends here




