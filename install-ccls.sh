#!/bin/bash

mkdir -p ~/Utilities
cd ~/Utilities

git clone --depth=1 --recursive https://github.com/MaskRay/ccls
cd ccls
cmake -H. -BRelease
cmake --build Release
